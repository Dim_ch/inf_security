﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lab
{
    class Program
    {
        static string fileOut = "e:\\textOut.txt";
        static void Main(string[] args)
        {
            Console.WriteLine("1 - аддитивный моноалфавитный шифр с задаваемым смещением");
            Console.WriteLine("2 - мультипликативный моноалфавитный шифр с задаваемым смещением");
            Console.WriteLine("3 - шифр Плейфера");
            int encryptionMode = GetMode(1, 3, "Выберите режим шифрования");

            Console.WriteLine("1 - ввод текста с консоли");
            Console.WriteLine("2 - получение текста из файла");

            int inputMode = GetMode(1, 2, "Выберите режим ввода");
            int key = 0;
            string text = "";

            switch(inputMode)
            {
                case 1:
                    text = GetText();
                    break;
                case 2:
                    text = GetText(GetFilePath());
                    break;
            }

            Cipher.Mod = Constants.alphabet.Length;
            string encodedText = "";
            switch (encryptionMode)
            {
                case 1:
                    key = GetMode(-35, 35, "Введите ключ");
                    Cipher.Key = key;
                    encodedText = Cipher.Ceaser(text);
                    break;
                case 2:
                    int count = 40;
                    KeyGen(Cipher.Mod, count);
                    key = GetMode(1, count, "Введите ключ");

                    Cipher.Key = key;
                    encodedText = Cipher.Multiplicative(text);
                    break;
                case 3:
                    Console.Write("Введите ключевую фразу: ");
                    Cipher.GenerateMatr(Console.ReadLine());
                    key = GetMode(1, 2, "Шифровать - 1\nДешифровать - 2\nВыберите режим");
                    encodedText = (key == 1) ? Cipher.PlayferEncoding(text) : Cipher.PlayferDecoding(text);
                    break;
            }
            PrintTextStatistic(encodedText);
            key = GetMode(1, 2, "Сохранить текст в файл - 1\nВывести текст в консоль - 2\nВыберите режим");

            if (key == 1) SaveToFile(GetFilePath(), encodedText);
            else Console.WriteLine("\nЗашифрованный текст:\n" + encodedText);
        }

        /// <summary>
        /// Получает режим работы с консоли
        /// </summary>
        /// <returns>возвращает 1 или 2</returns>
        static int GetMode(int from, int to, string mes)
        {
            string line;
            int mode;

            do
            {
                Console.Write($"{mes} от {from} до {to}: ");
                line = Console.ReadLine();

                while (!int.TryParse(line, out mode))
                {
                    Console.Write($"{mes} от {from} до {to}: ");
                    line = Console.ReadLine();
                }

            } while ((mode < from) || (mode > to));

            return mode;
        }

        /// <summary>
        /// Получает строку из консоли
        /// </summary>
        /// <returns></returns>
        static string GetText()
        {
            Console.Write("\nВведите текст: ");
            return Console.ReadLine();
        }

        /// <summary>
        /// Читает файл и возвращает строку
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static string GetText(string filePath)
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch(Exception error)
            {
                Console.WriteLine(error.Message);
            }
            return text;
        }

        /// <summary>
        /// Выводит на консоль возможные ключи для мультипликативного шифра
        /// </summary>
        /// <param name="mod">модуль</param>
        static void KeyGen(int mod, int count)
        {
            int index = 0;
            Console.WriteLine("Возможные ключи:");
            for (int i = 0; i < count; ++i)
            {
                for (int j = 0; j < count; ++j)
                {
                    if ((i * j % mod) == 1)
                    {
                        Console.Write($"Ключ {++index}: ({i}; {j})\n");
                    }
                }
            }
            Console.WriteLine();
        }

        static void PrintTextStatistic(string encodedText)
        {
            string text = encodedText.ToLower();
            IEnumerable<KeyValuePair<char, double>> statistics =  text.Where(x => Constants.alphabetRus.Contains(x))
                .GroupBy(x => char.ToLower(x))
                .Select(x => new KeyValuePair<char, double>(x.Key, (double)x.Count() / text.Length))
                .OrderByDescending(x => x.Value);

            Console.WriteLine("\nЧастота встречаемости символов закодированного текста:");
            foreach (KeyValuePair<char, double> item in statistics)
                Console.WriteLine($"{item.Key} - {string.Format("{0:f5}", item.Value)}");
        }

        /// <summary>
        /// Сохраняет текст в файл. При неудаче текст выводится в консоль
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="text"></param>
        static void SaveToFile(string filePath, string text)
        {
            try
            {
                using(StreamWriter writer = new StreamWriter(filePath, false))
                {
                    writer.Write(text);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                Console.WriteLine("Шифрованный текст:\n" + text);
            }
        }

        static string GetFilePath()
        {
            Console.Write("\nВведите путь к файлу: ");
            return Console.ReadLine();
        }
    }
}

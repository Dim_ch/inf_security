﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace lab8
{
    public class Program
    {
        public static void Main()
        {
            string filePath = "message.txt";
            string source = GetText(filePath);

            if (source.Equals("")) source = "Hello world";

            using (ECDsaCng dsa = new ECDsaCng())
            {
                dsa.HashAlgorithm = CngAlgorithm.Sha256;

                byte[] data = Encoding.UTF8.GetBytes(source);

                byte[] signature = dsa.SignData(data);

                Console.WriteLine($"Электронная подпись для сообщения '{source}': {GetHash(signature)}.");

                using (ECDsaCng ecsdKey = new ECDsaCng(CngKey.Import(dsa.Key.Export(CngKeyBlobFormat.EccPublicBlob),
                    CngKeyBlobFormat.EccPublicBlob)))
                {
                    if (ecsdKey.VerifyData(data, signature))
                        Console.WriteLine("Подпись является допустипой. (Сообщение не было изменено)");
                    else
                        Console.WriteLine("Подпись не является допустипой. (Сообщение было изменено)");
                }
            }
        }

        private static string GetHash(byte[] data)
        {
            var sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// Читает файл и возвращает строку
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static string GetText(string filePath)
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            return text;
        }
    }
}
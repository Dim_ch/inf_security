﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab
{
    public static class Cipher
    {
        static int key = 3;
        static int mod;
        static char[,] matr = new char[6, 6];

        public static int Key { get => key; set => key = value; }
        public static int Mod { get => mod; set => mod = value; }


        /// <summary>
        /// Выполняет шифр цезаря
        /// </summary>
        /// <param name="text">исходный текст</param>
        /// <returns>зашифрованный текст</returns>
        public static string Ceaser(string text)
        {
            string newText = "";

            for (int i = 0; i < text.Length; i++)
            {
                char ch = text[i];
                bool upper = Char.IsUpper(ch);
                int index = Constants.alphabet.IndexOf(ch);

                if (index != -1) {
                    int newIndex = (index + key) % mod;

                    if (newIndex < 0) newIndex = mod + newIndex;

                    ch = Constants.alphabet[newIndex];
                }

                if (upper) ch = Char.ToUpper(ch);
                newText += ch;
            }
            return newText;
        }

        public static string Multiplicative(string text)
        {
            string newText = "";
            
            for (int i = 0; i < text.Length; ++i)
            {
                char ch = text[i];
                bool upper = Char.IsUpper(ch);
                int index = Constants.alphabet.IndexOf(ch);

                if (index != -1)
                {
                    int newIndex = (index * key) % mod;

                    ch = Constants.alphabet[newIndex];
                }

                if (upper) ch = Char.ToUpper(ch);
                newText += ch;
            }

            return newText;
        }

        /// <summary>
        /// Кодирует текст, используя шифр Плейфера
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string PlayferEncoding(string text)
        {
            List<string> bigrams = GetBigramEncoding(text);

            int i_first, j_first,   //координаты первого символа пары
                i_second, j_second; //координаты второго символа пары

            string newText = "";
            char   ch1, ch2;
            bool   upper1, upper2;

            foreach (string bigram in bigrams)
            {
                ch1 = bigram[0];
                ch2 = bigram[1];
                upper1 = Char.IsUpper(ch1);
                upper2 = Char.IsUpper(ch2);

                if (upper1) ch1 = Char.ToLower(ch1);
                if (upper2) ch2 = Char.ToLower(ch2);

                GetPosition(ch1, ch2, out i_first, out j_first, out i_second, out j_second);

                if (i_first != -1 && j_first != -1 && i_second != -1 && j_second != -1 && ch1 != ch2)
                {
                    //если пара символов находится в одной строке
                    if (i_first == i_second)
                    {
                        j_first = (j_first == 5) ? 0 : j_first + 1;
                        j_second = (j_second == 5) ? 0 : j_second + 1;
                        ch1 = matr[i_first, j_first];
                        ch2 = matr[i_second, j_second];
                    }
                    //если пара символов находится в одном столбце
                    else if (j_first == j_second)
                    {
                        i_first = (i_first == 5) ? 0 : i_first + 1;
                        i_second = (i_second == 5) ? 0 : i_second + 1;
                        ch1 = matr[i_first, j_first];
                        ch2 = matr[i_second, j_second];
                    }
                    //если пара символов находиться в разных столбцах и строках
                    else
                    {
                        ch1 = matr[i_first, j_second];
                        ch2 = matr[i_second, j_first];
                    }
                }

                if (upper1) ch1 = Char.ToUpper(ch1);
                if (upper2) ch2 = Char.ToUpper(ch2);

                newText += ch1.ToString() + ch2.ToString();
            }
            return newText;
        }

        /// <summary>
        /// Декодирует текст по шифру Плейфера
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string PlayferDecoding(string text)
        {
            List<string> bigrams = GetBigramDecoding(text);

            int i_first, j_first,   //координаты первого символа пары
                i_second, j_second; //координаты второго символа пары

            string newText = "";
            char ch1, ch2;
            bool upper1, upper2;

            foreach (string bigram in bigrams)
            {
                ch1 = bigram[0];
                ch2 = bigram[1];
                upper1 = Char.IsUpper(ch1);
                upper2 = Char.IsUpper(ch2);

                if (upper1) ch1 = Char.ToLower(ch1);
                if (upper2) ch2 = Char.ToLower(ch2);

                GetPosition(ch1, ch2, out i_first, out j_first, out i_second, out j_second);

                if (i_first != -1 && j_first != -1 && i_second != -1 && j_second != -1 && ch1 != ch2)
                {
                    //если пара символов находится в одной строке
                    if (i_first == i_second)
                    {
                        j_first = (j_first == 0) ? 5 : j_first - 1;
                        j_second = (j_second == 0) ? 5 : j_second - 1;
                        ch1 = matr[i_first, j_first];
                        ch2 = matr[i_second, j_second];
                    }
                    //если пара символов находится в одном столбце
                    else if (j_first == j_second)
                    {
                        i_first = (i_first == 0) ? 5 : i_first - 1;
                        i_second = (i_second == 0) ? 5 : i_second - 1;
                        ch1 = matr[i_first, j_first];
                        ch2 = matr[i_second, j_second];
                    }
                    //если пара символов находиться в разных столбцах и строках
                    else
                    {
                        ch1 = matr[i_first, j_second];
                        ch2 = matr[i_second, j_first];
                    }
                }

                if (upper1) ch1 = Char.ToUpper(ch1);
                if (upper2) ch2 = Char.ToUpper(ch2);

                newText += ch1.ToString() + ch2.ToString();
            }

            return FixBigramDefect(newText);
        }

        private static void GetPosition(in char ch1, in char ch2, out int i1, out int j1, out int i2, out int j2)
        {
            i1 = i2 = j1 = j2 = -1;
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    //координаты первого символа пары в исходной матрице
                    if (ch1 == matr[i, j])
                    {
                        i1 = i;
                        j1 = j;

                    }

                    //координаты второго символа пары в исходной матрице
                    if (ch2 == matr[i, j])
                    {
                        i2 = i;
                        j2 = j;

                    }
                }
            }
        }

        /// <summary>
        /// Составляет матрицу для шифра Плейфера
        /// </summary>
        /// <param name="key">ключевая фраза</param>
        public static void GenerateMatr(string key)
        {
            int i = 0, j = 0;
            IEnumerable<char> distinctSymbol =  key.Distinct();
            foreach (char ch in distinctSymbol)
            {
                if (Constants.alphabet.Contains(ch))
                {
                    matr[i, j] = ch;
                    j++;
                    if (j == 6)
                    {
                        j = 0;
                        i++;
                        if (i == 6) break;
                    }
                }
            }

            foreach (char ch in Constants.alphabet)
            {
                if (!distinctSymbol.Contains(ch))
                {
                    matr[i, j] = ch;
                    j++;
                    if (j == 6)
                    {
                        j = 0;
                        i++;
                        if (i == 6) break;
                    }
                }
            }
        }

        /// <summary>
        /// Делит текст на биграммы и вставляет фиктивные символы при необходимости.
        /// </summary>
        /// <param name="text">входные данные</param>
        /// <returns>список строк-биграмм</returns>
        private static List<string> GetBigramEncoding(string text)
        {
            List<string> bigram = new List<string>();

            for (int i = 0; i < text.Length;)
            {
                if (i == text.Length - 1)
                {
                    bigram.Add(text[i].ToString() + "ф");
                    break;
                }
                // если символы совпадают, то добавляем фиктивную букву
                if (text[i] == text[i + 1])
                {
                    bigram.Add(text[i].ToString() + "ф");
                }
                else
                {
                    bigram.Add(text[i].ToString() + text[i + 1].ToString());
                    i++;
                }
                i++;
            }

            return bigram;
        }

        /// <summary>
        /// Получает биграмму для декодирования
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static List<string> GetBigramDecoding(string text)
        {
            List<string> bigram = new List<string>();

            if (text.Length % 2 != 0) text += " ";

            for (int i = 0; i < text.Length; i += 2)
            {
                bigram.Add(Char.ToString(text[i]) + Char.ToString(text[i + 1]));
            }

            return bigram;
        }

        /// <summary>
        /// Убирает из текста фиктивные символы
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static string FixBigramDefect(string text)
        {
            string newText = "";
            List<string> bigrams = GetBigramDecoding(text);

            foreach (string bigram in bigrams)
            {
                newText += bigram[0];
                if (bigram[1].ToString() != "ф") newText += bigram[1];
            }

            return newText;
        }
    }
}

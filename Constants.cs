﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab
{
    public static class Constants
    {
        public const string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъьыэюя_,.";
        public const string alphabetRus = "абвгдеёжзийклмнопрстуфхцчшщъьыэюя";
    }
}

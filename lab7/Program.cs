﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public class Program
{
    static byte[] hashValue = null;
    static byte[] signedHashValue = null;
    static RSAParameters rsaKeyInfo;

    public static void Main()
    {
        string filePath = "message.txt";
        string source = GetText(filePath);

        if (source.Equals("")) source = "Hello world";

        using (SHA256 sha256Hash = SHA256.Create())
        {
            string hash = GetHash(sha256Hash, source);

            Console.WriteLine($"Хэш (SHA256) строки '{source}': {hash}.");

            Console.WriteLine("Проверка хеш функций...");

            if (VerifyHash(sha256Hash, source, hash))
            {
                Console.WriteLine("Хэши одинаковы.");
            }
            else
            {
                Console.WriteLine("Хэши не одинаковы.");
            }

            string signed = getSignedRsa();

            Console.WriteLine($"Цифровая подпись: {signed}.");

            if (verifySigned()) {
                Console.WriteLine("Сигнатура подписи валидна.");
            }
            else
            {
                Console.WriteLine("Сигнатура подписи не валидна.");
            }
        }
    }

    private static string GetHash(HashAlgorithm hashAlgorithm, string input)
    {
        hashValue = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

        var sBuilder = new StringBuilder();

        for (int i = 0; i < hashValue.Length; i++)
        {
            sBuilder.Append(hashValue[i].ToString("x2"));
        }

        return sBuilder.ToString();
    }

    private static bool VerifyHash(HashAlgorithm hashAlgorithm, string input, string hash)
    {
        var hashOfInput = GetHash(hashAlgorithm, input);

        StringComparer comparer = StringComparer.OrdinalIgnoreCase;

        return comparer.Compare(hashOfInput, hash) == 0;
    }

    private static string getSignedRsa()
    {
        RSA rsa = RSA.Create();

        RSAPKCS1SignatureFormatter rsaFormatter = new RSAPKCS1SignatureFormatter(rsa);

        rsaFormatter.SetHashAlgorithm("SHA256");

        signedHashValue = rsaFormatter.CreateSignature(hashValue);

        rsaKeyInfo = rsa.ExportParameters(true);

        var sBuilder = new StringBuilder();

        for (int i = 0; i < signedHashValue.Length; i++)
        {
            sBuilder.Append(signedHashValue[i].ToString("x2"));
        }

        return sBuilder.ToString();
    }

    private static bool verifySigned()
    {
        RSA rsa = RSA.Create();
        rsa.ImportParameters(rsaKeyInfo);
        RSAPKCS1SignatureDeformatter rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
        rsaDeformatter.SetHashAlgorithm("SHA256");

        return rsaDeformatter.VerifySignature(hashValue, signedHashValue);
    }

    /// <summary>
    /// Читает файл и возвращает строку
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    static string GetText(string filePath)
    {
        string text = "";
        try
        {
            using (StreamReader sr = new StreamReader(filePath))
            {
                text = sr.ReadToEnd();
            }
        }
        catch (Exception error)
        {
            Console.WriteLine(error.Message);
        }
        return text;
    }
}
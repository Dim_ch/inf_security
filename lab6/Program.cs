﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;

public static class Program
{
    private static readonly char[] Characters = {
        '#', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К',
        'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш',
        'Щ', 'Ь', 'Ы', 'Ъ', 'Э', 'Ю', 'Я', ' ', '1', '2', '3', '4', '5', '6',
        '7', '8', '9', '0' };

    private static long p, q, d, n;

    private static void Main() {
        Console.WriteLine("Enter p");
        p = Convert.ToInt64(Console.ReadLine());
        Console.WriteLine("Enter q");
        q = Convert.ToInt64(Console.ReadLine());
        Encrypt();
        Decrypt();
    }

    private static void Encrypt() {
        if (IsTheNumberSimple(p) && IsTheNumberSimple(q)) { 
            string s = ""; 
            StreamReader sr = new StreamReader("in.txt");

            while (!sr.EndOfStream) {
                s += sr.ReadLine(); 
            }

            sr.Close();

            s = s.ToUpper(); 
            n = p * q; 
            long m = (p - 1) * (q - 1); 
            d = Calculate_d(m); 
            long e_ = Calculate_e(d, m);

            IEnumerable<string> result = RSA_Endoce(s, e_, n); 
            StreamWriter sw = new StreamWriter("out1.txt");

            foreach (string item in result)
                sw.WriteLine(item); 

            sw.Close(); 

            Console.WriteLine("d = {0}, n = {1}", d, n);
        }
        else 
            Console.WriteLine("p and q must be simple!"); } 
    //расшифровать
    private static void Decrypt() {
        var input = new List<string>();
        var sr = new StreamReader("out1.txt");

        while (!sr.EndOfStream) 
            input.Add(sr.ReadLine());

        sr.Close(); 

        string result = RSA_Dedoce(input, d, n);

        StreamWriter sw = new StreamWriter("out2.txt");

        sw.WriteLine(result);
        sw.Close();
    }

    private static bool IsTheNumberSimple(long dig) {
        switch (dig) { 
            case < 2: 
                return false; 
            case 2: 
                return true; 
        }

        for (long i = 2; i < dig; i++) if (dig % i == 0) return false; 

        return true;
    }

    private static IEnumerable<string> RSA_Endoce(string s, long e, long n) {
        return (
            from index in s.Select(t => Array.IndexOf(Characters, t)) 
            select new BigInteger(index) into bi 
            select BigInteger.Pow(bi, (int)e) into bi let n_ = new BigInteger((int)n) 
            select bi % n_ into bi select bi.ToString()).ToList(); 
    }

    static string RSA_Dedoce(IEnumerable<string> input, long d, long n) {
        return (from item in input select new BigInteger(Convert.ToDouble(item)) into bi 
                select BigInteger.Pow(bi, (int)d) into bi let n_ = new BigInteger((int)n)
                select bi % n_ into bi select Convert.ToInt32(bi.ToString())).Aggregate("", (current, index) => current + Characters[index]);
    }

    private static long Calculate_d(long m) {
        long d = m - 1;
        for (long i = 2; i <= m; i++) 
            if (m % i == 0 && d % i == 0) { 
                d--; 
                i = 1; 
            } 

        return d;
    }

    private static long Calculate_e(long d, long m) {
        long e = 10;
        while (true) {
            if (e * d % m == 1) 
                break; 
            e++;
        }

        return e;
    }
}
